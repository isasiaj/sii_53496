 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
 
 int main(){
   
     int fd;
     int data[2];

     
    if(mkfifo("/tmp/FIFOlogger",0666)<0){
        perror(" ERROR al crear fifo ");
        return 1;
        
    }
    
    fd = open ("/tmp/FIFOlogger",O_RDONLY);
    
    
    while(read(fd, data, sizeof(data)) == sizeof(data)){

         printf("Jugador %d marca 1 punto, lleva un total de %d puntos.\n",data[0],data[1]);
    }
    
    
     close(fd);
     unlink("/tmp/FIFOlogger");
     return(0);
 }
