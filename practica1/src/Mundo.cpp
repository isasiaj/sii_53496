// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
    munmap( (void *)p_bot1 ,sizeof(DatosMemCompartida));
    close(FIFOlogger);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for(int i=0;i< esfera.size();i++) esfera[i].Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
    p_bot1->esfera = esfera[0];
    p_bot1->raqueta1 = jugador1;
    
    
    
    
    
    time+= 0.025f;
    
    if(time >= 10){
        time=0;
        esfera.push_back( *(new Esfera));}
    
    
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
    for(int i=0;i<esfera.size();i++){
         esfera[i].Mueve(0.025f);
    }
	int i;
	for(i=0;i<paredes.size();i++)
	{
		for(int j=0;j<paredes.size();j++)paredes[i].Rebota(esfera[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
for(int j=0;j<paredes.size();j++){
	if(jugador1.Rebota(esfera[j])) jugador1.stun=0.5;
	if(jugador2.Rebota(esfera[j])) jugador2.stun=0.5;
}
    for(int j=0;j<paredes.size();j++){
if(fondo_izq.Rebota(esfera[j]))
	{
        int dataI[2]; 
        
        time=0;
		esfera[j].centro.x=0;
        esfera[j].radio=0.5;
		esfera[j].centro.y=rand()/(float)RAND_MAX;
		esfera[j].velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera[j].velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
        dataI[0]=2;
        dataI[1]=puntos2;
        
        write(FIFOlogger, dataI, sizeof(dataI));
	}

	if(fondo_dcho.Rebota(esfera[j]))
	{
        int dataD[2]; 
       
        time=0;
		esfera[j].centro.x=0;
        esfera[j].radio=0.5;
		esfera[j].centro.y=rand()/(float)RAND_MAX;
		esfera[j].velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera[j].velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
        dataD[0]=1;
        dataD[1]= puntos1;

        
        write(FIFOlogger, dataD, sizeof(dataD));
	}
    }
    // trabajo bots
    
    
    if (p_bot1->accion == -1) jugador1.velocidad.y=-4;
    if (p_bot1->accion == 1) jugador1.velocidad.y=4;

    if(puntos1==3){
        printf("GANA JUGADOR 1\n");
      exit(0);  
    }
    
      if(puntos2==3){
        printf("GANA JUGADOR 2\n");
        exit(0);
    }
    
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
        int fdbot1;
    // esferas
    esfera.push_back( *(new Esfera));
    
    
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
    
        //abrir fifo
   
    FIFOlogger = open("/tmp/FIFOlogger", O_WRONLY);
    if((FIFOlogger)<0){
        perror("No puede abrirse el FIFO");
        return;
    }
    
    

    // fichero a memoria
    
    fdbot1=open("/tmp/bot1",O_CREAT | O_RDWR  , 600);
    
     if(fdbot1<0){
        perror("Error al crear el fichero bot");
        return ;
    }
    
    ftruncate(fdbot1, sizeof(DatosMemCompartida));
    
    p_bot1 = (DatosMemCompartida *)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE ,MAP_SHARED,fdbot1,0);
    
        if(p_bot1==MAP_FAILED){
        perror("ERROR al proyectar fichero mundo");
        return;
    }
    
    
    
    
    close(fdbot1);

    
   
}
 
